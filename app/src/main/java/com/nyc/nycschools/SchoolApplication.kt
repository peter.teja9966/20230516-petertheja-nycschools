package com.nyc.nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by peterx.theja on 2023-05-14.
 */
@HiltAndroidApp
class SchoolApplication : Application() {

}