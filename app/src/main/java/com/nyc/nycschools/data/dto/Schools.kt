package com.nyc.nycschools.data.dto

/**
 * Created by peterx.theja on 2023-05-14.
 */
data class Schools(
    val schools: List<SchoolDto>
)
